###
# Copyright (c) 2009-2023, Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.conf as conf
import supybot.registry as registry
import Piccy


def configure(advanced):  # pylint: disable=unused-argument
    conf.registerPlugin("Piccy", True)


Piccy = conf.registerPlugin("Piccy")

conf.registerGlobalValue(
    Piccy,
    "db_hostname",
    registry.String("localhost", "hostname of kinfodb postgres database", private=True),
)
conf.registerGlobalValue(
    Piccy,
    "db_username",
    registry.String("kinfodb", "username of kinfodb postgres database", private=True),
)
conf.registerGlobalValue(
    Piccy,
    "db_password",
    registry.String("kinfodb", "password to kinfodb postgres database", private=True),
)
conf.registerGlobalValue(
    Piccy,
    "db_port",
    registry.Integer(5432, "port of kinfodb postgres database", private=True),
)
conf.registerGlobalValue(
    Piccy,
    "db_database",
    registry.String("kinfodb", "postgres to use", private=True)
)
conf.registerGlobalValue(
    Piccy,
    'use_conf_file',
    registry.Boolean(True, "use the kinfo.conf file in the plugin directory if present")
)

conf.registerGlobalValue(
    Piccy,
    "hcl_url",
    registry.String(
        "",  # formerly: "http://kmuto.jp/debian/hcl/index.rhtmlx?check=1&lspci=%s"
        "link for extra information about the pci-id; use %s for the pci-id; "
        "will not be included in output if set to empty string",
    ),
)
conf.registerGlobalValue(
    Piccy,
    "wiki_url",
    registry.String(
        "http://wiki.debian.org/%s",
        "link for extra user contributed information about the module; "
        "use %s for the module. "
        "Link will not be included in output if set to empty string",
    ),
)
conf.registerChannelValue(
    Piccy,
    "default_release",
    registry.String("stable", "default release to use for queries"),
)
conf.registerChannelValue(
    Piccy,
    "fallback_release",
    registry.String(
        "stable-backports",
        "fallback release kernel to look at for modules if not found in requested release",
    ),
)
conf.registerChannelValue(
    Piccy,
    "default_arch",
    registry.String("amd64", "default architecture to use for queries"),
)
conf.registerChannelValue(
    Piccy,
    "bold",
    registry.Boolean(
        True,
        "Determines whether the plugin will use bold in the "
        "responses to some of its commands.",
    ),
)

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
