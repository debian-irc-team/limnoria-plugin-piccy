###
# Copyright (c) 2009-2023 Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.conf as conf
from supybot.commands import wrap, getopts, many
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks

import re
import os

from debian.debian_support import Version

import kinfo.db
import kinfo.config
import kinfo.search

import uddcache.data


class Piccy(callbacks.Plugin):
    """A plugin for matching PCI-Ids with kernel modules and for looking up kernel config options"""
    threaded = True

    def __init__(self, irc):
        super().__init__(irc)
        self.db = self._fetch_kinfodb()
        self.conn = self.db.connect()
        self.udddata = uddcache.data.DebianData

    def _fetch_kinfodb(self):
        # first catch the test harness and simply use that if available
        if hasattr(conf, '_test_kinfodb_instance'):
            return conf._test_kinfodb_instance   # pylint: disable=no-member

        # If there is an kinfo.conf in the plugin directory then it
        # probably contains useful configuration data
        conffile = os.path.join(os.path.dirname(__file__), 'kinfo.conf')

        if os.path.isfile(conffile) and self.registryValue('use_conf_file'):
            self.log.debug("Using file kinfodb configuration: %s", conffile)
            kinfoconf = kinfo.config.Config(filename=conffile)
        else:
            self.log.debug("Using registry kinfodb configuration")
            # some amount of remapping of config option names is required
            confdict = {
                        'database': self.registryValue('db_database'),
                        'hostname': self.registryValue('db_hostname'),
                        'port': self.registryValue('db_port'),
                        'username': self.registryValue('db_username'),
                        'password': self.registryValue('db_password'),
                    }
            kinfoconf = kinfo.config.Config(confdict=confdict)

        # Initialise a database instance with the appropriate configuration
        return kinfo.db.KinfoDB(kinfoconf)

    def pciidHelper(self, irc, msg, args, pciid, optlist):
        """<pciid> [--release RELEASE] [ --arch ARCH]

        Output the name of the device, matching kernel module (if known),
        and packages containing firmware that module will load (if any).
        The current stable release and amd64 are queried by default.
        The pciid should be of the form 0000:0000. [ and ] are permitted around
        the pciid, but the bot will try to act on those as nested commands unless
        they are enclosed in double quotes, e.g "[0000:0000]".
        """
        channel = msg.args[0]
        release = self.udddata.clean_release_name(optlist=optlist,
                            default=self.default_release(channel))
        arch = self.udddata.clean_arch_name(optlist=optlist,
                            default=self.default_arch(channel))

        if not kinfo.search.pciid_check_format(pciid):
            irc.error("I don't know what you mean by PCI Id '%s'. 0000:0000 is my preferred format where both vendor Id and device Id are are in hexadecimal. You can find the PCI Id in the output of 'lspci -nn'." % pciid)
            return

        vendor, device = kinfo.search.clean_pciid_options(pciid=pciid, wide=False)

        vname, dname = kinfo.search.pciid_name_lookup(self.conn, vendor=vendor, device=device)
        if vname is None:
            vname = "Unknown vendor"
        if dname is None:
            dname = "Unknown device"

        try:
            modules, packages = kinfo.search.fetch_pciid_match(
                self.conn, release, arch, pciid, wildcards=False,
            )
        except ValueError:
            irc.error("sorry, no data for that release.")
            return

        if len(modules) == 1:
            moduletext = " with kernel module %s in %s/%s." % \
                            (self.bold(modules[0]), self.bold(release), self.bold(arch))
        elif len(modules) > 1:
            moduletext = " with kernel modules %s in %s/%s." % \
                            (self.boldCommaList(modules), self.bold(release), self.bold(arch))
        elif not modules:
            # no matching module found; let's try a fallback release
            modulefalltext=""
            fallback = self.udddata.clean_release_name(self.registryValue('fallback_release'))

            if release != fallback:
                try:
                    modules, packages = kinfo.search.fetch_pciid_match(
                        self.conn, fallback, arch, pciid, wildcards=False,
                    )
                    if not modules:
                        modulefalltext = " or in %s." % fallback
                    else:
                        modulefalltext = " but has kernel module(s) %s in %s/%s." % (self.boldCommaList(modules), self.bold(fallback), self.bold(arch))
                except ValueError:
                    # nothing to do here for the fallback
                    pass

            moduletext = " with no known kernel module in %s/%s%s." % (release, arch, modulefalltext)

        firmware = ""
        if packages:
            firmware = " Firmware in %s." % self.boldCommaList(packages)

        hcllink = ""
        if self.registryValue('hcl_url'):
            hcllink = self.registryValue('hcl_url') % pciid

        wikilink = ""
        if self.registryValue('wiki_url'):
            wikilink = " ".join(
                self.registryValue('wiki_url') % page
                for module in modules
                for page in kinfo.search.wiki_match_lookup(self.conn, module)
            )

        trailer = ""
        if hcllink or wikilink:
            trailer = " ".join([" See also", wikilink, hcllink])

        reply = f"[{vendor}:{device}] is '{dname}' from '{vname}'{moduletext}{firmware}{trailer}"

        irc.reply(reply)

    pciid = wrap(pciidHelper, [
        'something',
        getopts({'arch': 'something', 'release': 'something'})
    ])

    def modinfoHelper(self, irc, msg, args, module, optlist):
        """<module> [--release RELEASE] [ --arch ARCH]

        Output module info including packages from which firmware can be loaded.
        The current stable release and amd64 are queried by default.
        """
        channel = msg.args[0]
        release = self.udddata.clean_release_name(optlist=optlist,
                            default=self.default_release(channel))
        arch = self.udddata.clean_arch_name(optlist=optlist,
                            default=self.default_arch(channel))

        try:
            kinfo.search.module_lookup(self.conn, release, arch, module)
        except ValueError:
            irc.error("sorry, no data for that module.")
            return

        firmware_files = kinfo.search.firmware_lookup(self.conn, release, arch, module)
        packages = kinfo.search.firmware_packages_lookup(self.conn, release, arch, firmware_files)

        wiki_base = self.registryValue('wiki_url')
        wiki_pages = ""
        if wiki_base:
            wiki_page_list = kinfo.search.wiki_match_lookup(self.conn, module)
            wiki_pages = " ".join(wiki_base % w for w in wiki_page_list)

        if packages:
            packs = self.boldCommaList(set(packages))
            reply = f"Module '{module}' loads firmware from packages {packs}."
        else:
            reply = f"Module '{module}' does not load firmware from userspace."

        if wiki_pages:
            reply = f"{reply} See also: {wiki_pages}."

        irc.reply(reply)

    modinfo = wrap(modinfoHelper, [
        'something',
        getopts({'arch': 'something', 'release': 'something'})
    ])

    def firmwareHelper(self, irc, msg, args, firmware, optlist):
        """<firmware> [--release RELEASE] [ --arch ARCH]

        Output firmware package info. Specify the complete filename of the
        firmware file, without the path.
        The current stable release and amd64 are queried by default.
        """
        channel = msg.args[0]
        release = self.udddata.clean_release_name(optlist=optlist,
                            default=self.default_release(channel))
        arch = self.udddata.clean_arch_name(optlist=optlist,
                            default=self.default_arch(channel))

        packages = kinfo.search.firmware_packages_lookup(self.conn, release, arch, firmware)

        if packages:
            packs = self.boldCommaList(set(packages))
            reply = f"Firmware '{firmware}' can be found in {packs}."
        else:
            reply = f"Sorry, no packages were found with that firmware in {release}/{arch}."

        irc.reply(reply)

    firmware = wrap(firmwareHelper, [
        'something',
        getopts({'arch': 'something', 'release': 'something'})
    ])

    def pcinameHelper(self, irc, msg, args, name):
        """<device>

        Output possible full names and PCI-Ids of a device based on a partial
        name. (Minimum length 4 characters, special characters are not allowed)
        """
        min_length = 4       # minimum string length for the search

        origname = " ".join(name)

        # cleanse special characters from the search term to see how long it is
        name = re.sub(r'[^\s\w\d]', '', origname)

        if len(name) < min_length:
            irc.error("Please provide a search term that is at least %d characters long containing no special characters." % min_length)
            return

        devices = kinfo.search.pciname_match_lookup(self.conn, f"%{name}%")

        reply = ""
        if devices:
            devicelist = []
            for vid, did, vname, dname in devices:
                if vid.startswith("0000") and did.startswith("0000"):
                    vid = vid[4:8]
                    did = did[4:8]
                pciid = self.bold(f"[{vid}:{did}]")
                devicelist.append(f"{pciid} '{dname}' from '{vname}'")
            reply = "'%s' matched: %s" % (origname, ", ".join(devicelist))
        else:
            reply = "No devices were found that matched '%s'." % origname

        irc.reply(reply)

    pciname = wrap(pcinameHelper, [many('something')] )

    def kconfigHelper(self, irc, msg, args, pattern, optlist):
        """<config string> [--release RELEASE]  [--arch ARCH]

        Outputs the kernel configs that match the given string. By default
        the kernel from the stable release for amd64 is queried.
        The pattern is case insensitive and will have wildcards automatically added around it.
        """
        channel = msg.args[0]
        release = self.udddata.clean_release_name(optlist=optlist,
                            default=self.default_release(channel))
        arch = self.udddata.clean_arch_name(optlist=optlist,
                            default=self.default_arch(channel))

        if pattern.startswith("CONFIG_"):
            pattern = pattern[7:]

        origpattern = pattern
        if not pattern.startswith("%"):
            pattern = f"%{pattern}"

        if not pattern.endswith("%"):
            pattern = f"{pattern}%"

        min_length = 4       # minimum string length for the search

        # cleanse special characters from the search term to see how long it is
        if len(pattern.replace("%", "").replace("?", "")) < min_length:
            irc.error("Please provide a search term that is at least %d characters long containing no special characters." % min_length)
            return

        configlist = kinfo.search.kconfig_lookup(self.conn, release, arch, pattern)

        if configlist is None:
            irc.error("Error looking up config list for release %s." % release)
            return

        if len(configlist) == 0:
            configtext = self.bold('no results')
        else:
            configtext = ' '.join("=".join(c) for c in configlist)

        reply = "Searching for '%s' in %s kernel config gives %s." % (self.bold(origpattern), self.bold(release), configtext)

        irc.reply(reply)

    kconfig = wrap(kconfigHelper, [
        'something',
        getopts({'arch': 'something', 'release': 'something'})
    ])

    # provide a command alias as well
    kernelconfig = kconfig


    def kernelVersionHelper(self, irc, msg, args, optlist):
        """[--release RELEASE] [--arch ARCH]

        Outputs the kernel versions in the archive for the specified architecture
        and optionally restricted to one release. Note that backports and
        security updates are separate releases.
        """
        channel = msg.args[0]
        release = self.udddata.clean_release_name(optlist=optlist,
                            default=None)
        arch = self.udddata.clean_arch_name(optlist=optlist,
                            default=self.default_arch(channel))

        versions = kinfo.search.kernel_lookup(self.conn, release, arch)

        # sort the returned versions based on the udd release list
        release_map = {name: num for num, name in enumerate(self.udddata.releases)}

        sorted_verions = sorted(
            versions,
            key=lambda v: (Version(v["version"]), release_map.get(v["release"], -1))
        )

        versionstrings = []
        if versions:
            for v in sorted_verions:
                versionstrings.append("%s: %s (%s)"
                                    % (self.bold(v["release"]),
                                       v["uname"],
                                       v["version"]))
            reply = "Available kernel versions are: " + ("; ".join(versionstrings))
        else:
            reply = "No kernel versions found."

        irc.reply(reply)

    kernel = wrap(kernelVersionHelper, [
        getopts({'arch': 'something', 'release':'something'})
    ])

    # provide convenience aliases for kernel version command
    kernels = kernel

    def boldCommaList(self, listing, fmt="%s"):
        """
        Return the list comma separated, with each term in bold
        """
        return ", ".join(
                map(lambda m: fmt % self.bold(m), listing)
              )

    def bold(self, s):
        """return the string in bold markup if required"""
        if self.registryValue('bold', dynamic.channel):  # pylint: disable=undefined-variable
            return ircutils.bold(s)
        return s

    def default_release(self, channel):
        """the release that is the default for the current channel"""
        return self.registryValue('default_release', channel)

    def default_arch(self, channel):
        """the architecture that is the default for the current channel"""
        return self.registryValue('default_arch', channel)

Class = Piccy

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
