###
# Copyright (c) 2009-2021 Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

# Note that the comments for each test are structured as:
#
#    self.assertNotError('test command')  # short test description ; test result
#
# this is designed to allow the tests to be run semi-interactively through
# some RPC interaction with an IRC client.

import unittest

import supybot.test
import supybot.conf as conf

from kinfo.tests.conftest import postgresql_templated_proc, postgresql, fixture_kinfo_db  # pylint: disable=unused-import
from kinfo.db import KinfoDB

import pytest

from pytest_limnoria_plugin import PytestPluginTestCase


@pytest.mark.usefixtures("kinfo_db", "limnoria")
class PiccyPluginTestCase(PytestPluginTestCase):
    plugins = ('Piccy',)
    timeout = 180.  # bump timeout up to 180s

    # from the kinfo_db fixture
    db: KinfoDB
    #connman: Connection
    kernel_version: str
    kernel_uname: str
    release: str
    architecture: str

    def setUp(self, nick='test', forceSetup=False):
        # hack the kinfo db connection from the pytest fixture into the
        # conf since that will easily make it available to the plugin
        conf._test_kinfodb_instance = self.db
        # Set the release that is used for the sample data in the db
        conf.supybot.plugins.Piccy.default_release.setValue(self.release)
        # FIXME: need to broaden the test db to have two releases
        conf.supybot.plugins.Piccy.fallback_release.setValue(self.release)
        super().setUp(nick, forceSetup)

    def test_pciname(self):
        # FIXME test that the output is correct too
        self.assertNotError('pciname BCM5716S')     # single hit; matches [14E4:163D] NetXtreme II BCM57811 Gigabit Ethernet
        self.assertRegexp(
            "pciname BCM5716S",
            r"BCM5716S.*\[14E4:163C\].*NetXtreme.*Broadcom"
        )
        self.assertNotError('pciname NetXtreme')       # multiple hits; matches [14E4:163D] NetXtreme II BCM57811 10-Gigabit Ethernet and 2 other devices
        matches = str(self.getMsg("pciname NetXtreme"))
        self.assertGreaterEqual(matches.count("NetXtreme"), 4)
        self.assertGreaterEqual(matches.count("Broadcom"), 4)
        self.assertIn("[14E4:16", matches)
        matches = str(self.getMsg("pciname netxtreme"))
        self.assertGreaterEqual(matches.count("NetXtreme"), 4)
        self.assertNotError('pciname BCM57811')    # match not at start of devname; [14E4:163D] NetXtreme II BCM57811 10-Gigabit Ethernet and 2 other devices
        self.assertRegexp(
            "pciname BCM57811",
            r"BCM57811.*\[14E4:163D\].*NetXtreme.*Broadcom"
        )
        self.assertError('pciname 578')           # minimum search string length; at least 4 chars in required in search
        self.assertError('pciname 5..1')          # minimum search string length; at least 4 chars in required in search (will sanitised down to 87 which is then an error)
        self.assertNotError('pciname BCM57.*')    # filter bad characters ; 4 matches anyway
        self.assertRegexp(
            "pciname BCM57.*",
            r"BCM57.*\[14E4:16.*NetXtreme.*Broadcom"
        )
        self.assertNotError('pciname BCM5716S Gigabit')    # single space in arguments; matches [14E4:163D] NetXtreme II BCM57811 Gigabit Ethernet
        self.assertRegexp(
            "pciname BCM5716S Gigabit",
            r"BCM57.*\[14E4:16.*NetXtreme.*Broadcom"
        )
        self.assertNotError('pciname BCM5716S   Gigabit')    # multiple spaces in arguments; matches [14E4:163D] NetXtreme II BCM57811 Gigabit Ethernet
        self.assertNotError('pciname BCM5716S   Gigabit')    # single space in arguments; matches [14E4:163D] NetXtreme II BCM57811 Gigabit Ethernet
        self.assertRegexp(
            "pciname BCM5716S Gigabit",
            r"BCM57.*\[14E4:16.*NetXtreme.*Broadcom"
        )
        self.assertNotError('pciname BCM5716S gigabit')    # case sensitivity; matches [14E4:163D] NetXtreme II BCM57811 Gigabit Ethernet
        self.assertHelp('pciname')

    def test_pciid(self):
        # FIXME test that the output is correct too
        self.assertNotError('pciid 14e4:163c')        # simple test ; 'NetXtreme II BCM5716S Gigabit Ethernet' to b44 module with no wiki link
        self.assertNotError('pciid 14E4:163C')        # case insensitive; 'NetXtreme II BCM5716S Gigabit Ethernet' to b44 module with no wiki link
        for q in ["pciid 14e4:163c", "pciid 14e4:163c"]:
            reply = str(self.getMsg(q))
            self.assertIn("[14E4:163C]", reply)
            self.assertIn("NetXtreme", reply)
            self.assertIn("Broadcom", reply)
            self.assertIn("bnx2", reply)
            self.assertIn("firmware-bnx2", reply)
            self.assertIn("wiki.debian.org/wiki-bnx2-page", reply)
        self.assertNotError('pciid 8086:0089')        # with wiki link; 'Centrino Advanced-N + WiMAX 6250 [Kilmer Peak]' to iwlwifi module with wiki link
        reply = str(self.getMsg("pciid 8086:0089"))
        self.assertIn("Intel", reply)
        self.assertNotError('pciid "[8086:0089]"')        # in brackets; 'Centrino Advanced-N + WiMAX 6250 [Kilmer Peak]' to iwlwifi module with wiki link
        reply = str(self.getMsg('pciid "[8086:0089]"'))
        self.assertIn("Intel", reply)
        # FIXME: release and arch testing
        #self.assertNotError('pciid 8086:27b9')        # multiple module matches ; intel-rng and iTCO_wdt
        #self.assertNotError('pciid 8086:4229')        # multiple wikifaq matches ; wikifaq: iwlwifi iwlagn
        #self.assertNotError('pciid 10de:1234')        # PCI_ID_ANY module ; nvidia device with PCI_ID_ANY in map for out-of-tree nvidia driver
        ##self.assertNotError('pciid 1904:8139 --release etch')      # fallthru to sid ; RTL8139D has no match in etch, matches sc92031 in sid
        #self.assertNotError('pciid "[8086:4222]"')    # pciid wrapped in brackets ; 'PRO/Wireless 3945ABG [Golan] Network Connection'. Should include wikilink for iwlwifi.
        #self.assertNotError('pciid "[8086:4222]" --release unstable') # check driver in sid ; 'PRO/Wireless 3945ABG [Golan] Network Connection'. Should include wikilink for iwlwifi.
        #self.assertNotError('pciid "[1002:7145]"')    # non-free driver ; 'Radeon Mobility X1400' with in kernel ati-agp module and out-of-tree fglrx and wikilink for fglrx
        self.assertNotError('pciid "001c:0001"')      # no matches ; no matches in any distro
        self.assertNotError('pciid ffff:0001')        # illegal vendor ID ; unknown device from illegal vendor id
        self.assertNotError('pciid 0069:0001')  # unknown vendor ID ; 'Unknown device' from 'Unknown vendor'
        self.assertError('pciid 1:0011')        # malformed pciid ; vendor id too short
        self.assertError('pciid 123:0011')      # malformed pciid ; vendor id too short
        self.assertError('pciid qwe1:0011')     # malformed pciid ; vendor id bad characters
        self.assertError('pciid 001c:123q')     # malformed pciid ; device id bad characters
        self.assertHelp('pciid')

    def test_kconfig(self):
        self.assertNotError('kconfig CONFIG_SCSI_BNX2X_FCOE')    # single hit ; returns single hit, full module name
        self.assertRegexp('kconfig CONFIG_SCSI_BNX2X_FCOE', "SCSI_BNX2X_FCOE=m")
        self.assertNotError('kconfig SCSI_BNX2X_FCOE')    # single hit ; returns single hit, partial module name
        self.assertRegexp('kconfig SCSI_BNX2X_FCOE', "SCSI_BNX2X_FCOE=m")
        self.assertNotError('kconfig BNX2')      # multiple hits ; several partial matches
        self.assertNotError('kconfig bnx2')      # case sensitive ; case-insensitive matching, several partial matches

        for q in ["BNX2", "bnx2"]:
            reply = str(self.getMsg("kconfig " + q))
            self.assertIn("BNX2=m", reply)
            self.assertIn("BNX2X=m", reply)

        self.assertNotError('kconfig foobar')    # no match ; fails to match
        self.assertRegexp('kconfig foobar', "no results")
        # FIXME: check values in specific releases
        #self.assertNotError('kconfig CONFIG_SMB_FS --release etch')  # match in release ; only set in etch
        #self.assertNotError('kconfig CONFIG_SMB_FS --release lenny') # match in release ; not set in lenny
        self.assertError("kconfig BNX")
        self.assertError("kconfig BNX%")
        self.assertError("kconfig %BNX")
        self.assertHelp("kconfig")

    def test_firmware(self):
        self.assertRegexp("firmware bnx2-rv2p-09ax-6.0.17.fw", "firmware-bnx2")
        self.assertRegexp("firmware no-such-file", "Sorry")
        # FIXME: release and arch testing
        self.assertHelp("firmware")

    def test_modinfo(self):
        self.assertRegexp("modinfo bnx2", "firmware-bnx2")
        self.assertRegexp("modinfo bnx2nofw", "does not load")
        self.assertRegexp("modinfo iwlwifi", "wiki.debian.org/iwlwifi")
        self.assertRegexp("firmware no-such-file", "Sorry")
        # FIXME: release and arch testing
        self.assertHelp("modinfo")

    def test_kernels(self):
        self.assertNotError('kernels')    # kernel versions ; returns all versions from oldstable to trunk
        self.assertNotError(f'kernels --release {self.release}')  # single release ; returns a single release
        # FIXME: release and arch testing
        self.assertNotError('kernel')     # kernels alias ; returns all versions

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
